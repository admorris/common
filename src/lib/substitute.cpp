#include "substitute.h"
std::string substitute(std::string source, const std::string& find, const std::string& replace)
{
	for(std::string::size_type i = 0; (i = source.find(find, i)) != std::string::npos;)
	{
		source.replace(i, find.length(), replace);
		i += replace.length();
	}
	return source;
}
