// Custom headers
#include "GetTree.h"
#include "MakeBranchPlot.h"
#include <algorithm>
#include <iostream>
TH1D* MakeBranchPlot(TTree& tree, std::string branchname, std::string cuts, std::string weight, double xlow, double xup, int nbins, bool autorange)
{
	std::string histname = branchname+"hist";
	histname.erase(std::remove_if(histname.begin(), histname.end(), &ispunct), histname.end());
	if(autorange)
	{
		xlow = tree.GetMinimum(branchname.c_str());
		xup = tree.GetMaximum(branchname.c_str());
	}
	TH1D* hist = new TH1D(histname.c_str(),"",nbins,xlow,xup);
	if(cuts != "")
	{
		if(weight != "")
			weight += "*(" + cuts + ")";
		else
			weight = cuts;
	}
	tree.Draw((branchname+">>"+hist->GetName()).c_str(),weight.c_str(),"goff");
	hist->SetLineColor(kBlack);
	hist->SetMarkerStyle(20);
	return hist;
}
TH1D* MakeBranchPlot(TFile& file, std::string branchname, std::string cuts, std::string weight, double xlow, double xup, int nbins, bool autorange)
{
	return MakeBranchPlot(*GetTree(&file), branchname, cuts, weight, xlow, xup, nbins, autorange);
}
TH1D* MakeBranchPlot(std::string filename, std::string branchname, std::string cuts, std::string weight, double xlow, double xup, int nbins, bool autorange)
{
	return MakeBranchPlot(*GetTree(filename), branchname, cuts, weight, xlow, xup, nbins, autorange);
}
TH2D* MakeBranchPlot2D(TTree& tree, std::string xbranch, std::string ybranch, std::string cuts, std::string weight, double xlow, double xup, int nxbins, double ylow, double yup, int nybins)
{
	std::string histname = xbranch+"hist";
	histname.erase(std::remove_if(histname.begin(), histname.end(), &ispunct), histname.end());
	TH2D* hist = new TH2D(histname.c_str(),"",nxbins,xlow,xup,nybins,ylow,yup);
	if(cuts != "")
	{
		if(weight != "")
			weight += "*(" + cuts + ")";
		else
			weight = cuts;
	}
	tree.Draw((ybranch+":"+xbranch+">>"+hist->GetName()).c_str(),weight.c_str(),"COLZ goff");
	return hist;
}
TH2D* MakeBranchPlot2D(TFile& file, std::string xbranch, std::string ybranch, std::string cuts, std::string weight, double xlow, double xup, int nxbins, double ylow, double yup, int nybins)
{
	return MakeBranchPlot2D(*GetTree(&file), xbranch, ybranch, cuts, weight, xlow, xup, nxbins, ylow, yup, nybins);
}
TH2D* MakeBranchPlot2D(std::string filename, std::string xbranch, std::string ybranch, std::string cuts, std::string weight, double xlow, double xup, int nxbins, double ylow, double yup, int nybins)
{
	return MakeBranchPlot2D(*GetTree(filename), xbranch, ybranch, cuts, weight, xlow, xup, nxbins, ylow, yup, nybins);
}

