#include "TLatex.h"
#include "TMath.h"
#include "TAxis.h"
#include "TStyle.h"
#include "RooHist.h"
#include "plotmaker.h"
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
using std::stringstream;
/*****************************************************************************/
// Constructor
template<class T>
plotmaker<T>::plotmaker(T* mainplot) :
	_mainplot(mainplot)
{
	_mainplot->SetTitle("");
	_usepull = false;
	getaxes(_mainplot,false);
	init();
}
// Make sure to delete all the "new" objects
template<class T>
plotmaker<T>::~plotmaker()
{
	if(_blurb != NULL)
	{
		delete _blurb;
	}
	delete _mainpad;
	if(_usepull)
	{
		delete _pullpad;
	}
	delete _canvas;
}
/*****************************************************************************/
// Set default values for variables and set things up for drawing.
template<class T>
void plotmaker<T>::init()
{
	_blurbx				 = 0.75;
	_blurby				 = 0.80;
	_blurbtext		 = "";//"#splitline{LHCb}{#scale[0.75]{Preliminary}}";
	_xtitle				 = "#it{m}(#it{K^{#plus}K^{#minus}K^{#plus}K^{#minus}})";
	_unit					 = "MeV/#it{c}^{2}";
	_dimensionless = false;
	_logy					 = false;
	makepads();
}
template<class T>
void plotmaker<T>::getaxes(T* plot, bool pull)
{
	if(pull)
	{
		_pullxaxis = plot->GetXaxis();
		_pullyaxis = plot->GetYaxis();
	}
	else
	{
		_mainxaxis = plot->GetXaxis();
		_mainyaxis = plot->GetYaxis();
	}
}
template<class T>
void plotmaker<T>::makepads()
{
	if(_usepull)
	{
		_canvas = new TCanvas("canvas","",1200,1000);
		_canvas->Draw();
		_mainpad = new TPad("mainpad", "", 0.00, 0.3, 1.00, 1.00);
		_mainpad->SetMargin(0.15,0.05,0.03,0.05);
		_mainpad->SetTicks(1,1);
		_mainpad->Draw();
		_pullpad = new TPad("pullpad", "", 0.00, 0.00, 1.00, 0.3);
		_pullpad->SetMargin(0.15,0.05,0.50,0.02);
		_pullpad->SetTicks(1,1);
		_pullpad->Draw();
	}
	else
	{
		_canvas = new TCanvas("canvas","",1200,900);
		_canvas->Draw();
		_mainpad = new TPad("mainpad", "", 0.00, 0.00, 1.00, 1.00);
		_mainpad->SetMargin(0.15,0.05,0.2,0.05);
		_mainpad->SetTicks(1,1);
		_mainpad->Draw();
	}
}
/*****************************************************************************/
template<class T>
void plotmaker<T>::SetDrawOption(string drawopt)
{
	_drawopt = drawopt;
}
/*****************************************************************************/
template<class T>
void plotmaker<T>::SetBlurb(string text)
{
	_blurbtext = text;
}
template<class T>
void plotmaker<T>::SetBlurbPosition(double x, double y)
{
	_blurbx = x;
	_blurby = y;
}
/*****************************************************************************/
template<class T>
void plotmaker<T>::SetPullPlot(T* pullplot)
{
	_pullplot = pullplot;
	_pullplot->SetTitle("");
	getaxes(_pullplot,true);
	_usepull = true;
	_pullplot->SetDrawOption("BX");	// Make symmetric around zero
//	double max = _pullplot->GetMaximum();
//	double min = _pullplot->GetMinimum()*-1;
	double newmax = 5;//1.2*(1+(int)std::max(max,min));
	_pullplot->SetMaximum(newmax);
	_pullplot->SetMinimum(-newmax);
	makepads();
}
/*****************************************************************************/
template<class T>
void plotmaker<T>::SetTitle(string xtitle, string unit)
{
	replace(xtitle.begin(), xtitle.end(), '~', ' ');
	_xtitle = xtitle;
	_unit = unit;
	transform(unit.begin(), unit.end(), unit.begin(), ::tolower);
	_dimensionless = (unit == "dimensionless" || unit == "unitless" || unit == "none" || unit == "");
}
/*****************************************************************************/
template<class T>
void plotmaker<T>::stylemainaxis(TAxis* axis)
{
	axis->SetTitleFont(132);
	axis->SetLabelFont(132);
	axis->SetNdivisions(505);
	axis->SetTitleOffset(1.10);
	axis->SetTitleSize	(0.065);
	axis->SetLabelSize	(0.055);
	axis->SetLabelOffset(0.015);
}
template<class T>
void plotmaker<T>::setxtitle(TAxis* axis)
{
	if(_dimensionless)
	{
		axis->SetTitle(("#font[132]{}"+_xtitle).c_str());
	}
	else
	{
		axis->SetTitle(("#font[132]{}"+_xtitle+" #font[132]{}["+_unit+"]").c_str());
	}
}
template<class T>
void plotmaker<T>::setytitle(TAxis* axis,double binw)
{
	axis->CenterTitle();
	stringstream ytitle;
	ytitle << "#font[132]{}Candidates / (";
	if(binw > 10)
		ytitle << TMath::Nint(binw);
	else if (binw > 0.1)
		ytitle << TMath::Nint(binw*10.0)/10.0;
	else
		ytitle << TMath::Nint(binw*pow(10.0,ceil(-log10(binw))))*pow(10.0,floor(log10(binw)));
	if(!_dimensionless)
	{
		ytitle << " " << _unit;
	}
	ytitle << ")";
	axis->SetTitle(ytitle.str().c_str());
}
template<class T>
void plotmaker<T>::stylepullaxes(TAxis* xaxis, TAxis* yaxis)
{
	stylemainaxis(xaxis);
	stylemainaxis(yaxis);
	// Axis titles
	yaxis->SetTitle("Pull");
	yaxis->CenterTitle();
	xaxis->SetTitleOffset(1.20);
	yaxis->SetTitleOffset(0.40);
	xaxis->SetTitleSize	 (0.17);
	yaxis->SetTitleSize	 (0.17);
	// Axis labels
	xaxis->SetLabelSize	 (0.15);
	yaxis->SetLabelSize	 (0.10);
	// Ticks
	xaxis->SetTickLength(0.1);
	yaxis->SetNdivisions(5);
}
template<class T>
void plotmaker<T>::drawblurb()
{
	_blurb = new TLatex(_blurbx,_blurby,_blurbtext.c_str());
	_blurb->SetTextFont(132);
	_blurb->SetTextColor(1);
	_blurb->SetNDC(kTRUE);
	_blurb->SetTextAlign(11);
	_blurb->SetTextSize(0.07);
	_blurb->Draw();
}
template<class T>
void plotmaker<T>::drawplot(T* plot, string option)
{
	plot->Draw(option.c_str());
}
/*****************************************************************************/
template<class T>
TCanvas* plotmaker<T>::Draw(string option)
{
	if(option != "") SetDrawOption(option);
	gStyle->SetOptStat(0);
	gStyle->SetOptFit(0);
	setxtitle(_usepull ? _pullxaxis : _mainxaxis);
	setytitle(_mainyaxis,_mainxaxis->GetBinWidth(1));
	stylemainaxis(_mainxaxis);
	stylemainaxis(_mainyaxis);
	// Pull frame style
	if(_usepull)
	{
		// Get rid of the stuff under the main plot
		_mainxaxis->SetLabelSize(0);
		stylepullaxes(_pullxaxis,_pullyaxis);
		// Finish
		_pullpad->cd();
		drawplot(_pullplot,_drawopt);
	}
	_mainpad->cd();
	if(_logy) _mainpad->SetLogy();
	drawplot(_mainplot,_drawopt);
	_mainpad->cd();
	drawblurb();
	return _canvas;
}
/*****************************************************************************/
template<class T>
TH1D plotmaker<T>::MakePullPlot(TH1D* exp, TH1D* obs)
{
	TH1D pullplot(*obs);
	pullplot.SetName("pullplot");
	pullplot.Add(exp,-1);
	pullplot.SetFillColor(kBlack);
	normaliseresiduals(&pullplot, exp, obs);
	return pullplot;
}
/*****************************************************************************/
template<class T>
void plotmaker<T>::normaliseresiduals(TH1* plot, const TH1* exp, const TH1* obs)
{
	int nbins = plot->GetNbinsX();
	for(int ibin = 1; ibin < nbins+1; ibin++)
	{
		double obsbinerr = obs->GetBinError(ibin);
		double expbinerr = exp->GetBinError(ibin);
		double binerr = sqrt(obsbinerr*obsbinerr + expbinerr*expbinerr);
		if(std::abs(binerr)>1e-12)
			plot->SetBinContent(ibin, plot->GetBinContent(ibin)/binerr);
	}
}
/*****************************************************************************/
template<class T>
TCanvas* plotmaker<T>::DrawPullDistribution(bool fit)
{
	int n = GetNbins(_pullplot);
	// Binned data for plotting
	TH1D* distribution = new TH1D("pull_dist","",20,-5,5);
	for(int i = 0; i < n; ++i)
	{
		auto pull = GetY(_pullplot, i);
		distribution->Fill(pull);
	}
	TCanvas* can = new TCanvas("pull_dist_can","");
	distribution->Draw("E");
	if(fit)
	{
		distribution->Fit("gaus");
	}
	return can;
}
/*Get the number of bins******************************************************/
template<class T>
int plotmaker<T>::GetNbins(T* plot)
{
	return plot->GetNbinsX();
}
// TGraph has a differently-named method
template<>
int plotmaker<TGraph>::GetNbins(TGraph* plot)
{
	return plot->GetN();
}
/*Get the y-value of a certain bin********************************************/
template<class T>
auto plotmaker<T>::GetY(T* plot, int i)
{
	return plot->GetBinContent(i+1);
}
template<>
auto plotmaker<TGraph>::GetY(TGraph* plot, int i)
{
	double x,y;
	plot->GetPoint(i,x,y);
	return y;
}
template<>
auto plotmaker<RooPlot>::GetY(RooPlot* plot, int i)
{
	return plotmaker<TGraph>::GetY(plot->getHist(), i);
}
/*Get the y-error of a certain bin********************************************/
template<class T>
auto plotmaker<T>::GetEY(T* plot, int i)
{
	return plot->GetBinError(i+1);
}
template<>
auto plotmaker<TGraph>::GetEY(TGraph* plot, int i)
{
	return plot->GetErrorY(i);
}
template<>
auto plotmaker<RooPlot>::GetEY(RooPlot* plot, int i)
{
	return plotmaker<TGraph>::GetEY(plot->getHist(), i);
}
/*****************************************************************************/
// Tell the compiler which classes to compile
template class plotmaker<TH1>;
template class plotmaker<TGraph>;
template class plotmaker<RooPlot>;
