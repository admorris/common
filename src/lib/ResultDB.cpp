#include <fstream>
#include <iostream>
#include <iomanip>
#include <regex>
#include "ResultDB.h"
namespace rdb
{
	int order(double value)
	{
		if(std::abs(value)<1e-100) return 0;
		return floor(log10(std::abs(value)));
	}
	double roundSF(double value, int nsf)
	{
		if(std::abs(value)<1e-100) return 0;
		if(nsf<1) return 0;
		double factor = pow(10.0, nsf - ceil(log10(std::abs(value))));
		return round(value * factor) / factor;
	}
	double roundDP(double value, int ndp)
	{
		double factor = pow(10.0, ndp);
		return round(value * factor) / factor;
	}
	template<typename T>
	std::string tostring(T value, int prec)
	{
		std::stringstream s;
		s << std::setprecision(prec) << std::fixed << value;
		return s.str();
	}
	std::string scinot(double value, int ndp)
	{
		int ov = order(value);
		value /= pow(10.,ov);
		return tostring(roundDP(value,ndp), ndp) + "\\times 10^{" + tostring(ov,0) + "}";
	}
	std::string scinot(double value, double error, int ndp)
	{
		int o = order(value);
		value /= pow(10.,o);
		error /= pow(10.,o);
		return "(" + tostring(roundDP(value,ndp), ndp) + " \\pm " + tostring(roundDP(error,ndp), ndp) + ")"+ "\\times 10^{" + tostring(o, 0) + "}";
	}
}

result::result()
	: name("temp")
	, type("decimal")
	, value(0)
	, error(0)
{

}
result::result(std::string n, std::string t, double v, double e)
	: name(n)
	, type(t)
	, value(v)
	, error(e)
{

}
void result::set(std::string t, double v, double e)
{
	type = t;
	value = v;
	error = e;
}

ResultDB::ResultDB()
	: _isopen(false)
	, _ismod(false)
{

}
ResultDB::ResultDB(std::string filename)
	: _isopen(false)
	, _ismod(false)
{
	Open(filename);
}
ResultDB::~ResultDB()
{
	Close();
}
bool ResultDB::Open(std::string filename)
{
	if(_isopen)
	{
		std::cerr << "Cannot open " << filename << " because " << _filename << " is already open. Close it first." << std::endl;
		return false;
	}
	_filename = filename;
	std::ifstream input(filename);
	if(!input.is_open())
	{
		std::cerr << _filename << " not found. The file will be created if Save() is called without argument." << std::endl;
		_isopen = true;
		return true;
	}
	do
	{
		result row;
		input >> row.name >> row.type >> row.value >> row.error;
		if(row.name.compare("temp") != 0)  // why is this here?
			_table.push_back(row);
	} while(!input.eof());
	input.close();
	_isopen = true;
	return true;
}
bool ResultDB::Save(std::string filename)
{
	if(!_isopen)
	{
		std::cerr << "Nothing to save." << std::endl;
		return false;
	}
	std::ofstream output;
	if(filename != "")
		output.open(filename);
	else if(_filename != "")
		output.open(_filename);
	else
	{
		std::cerr << "No filename specified." << std::endl;
		return false;
	}
	for(auto row : _table)
		output << row.name << "\t" << row.type << "\t" << row.value << "\t" << row.error << std::endl;
	output.close();
	_ismod = false;
	return true;
}
bool ResultDB::Close()
{
	if(_isopen)
		_table.clear();
	_isopen = false;
	if(_ismod)
		std::cerr << "Closed without saving. Changes lost." << std::endl;
	_ismod = false;
	return true;
}
void ResultDB::Update(std::string name, std::string type, double value, double error)
{
	if(name.compare("") == 0) name = "temp";
	if(type.compare("") == 0) type = "default";
	find(name)->set(type, value, error);
	_ismod = true;
}
result* ResultDB::find(std::string name)
{
	for(auto& row : _table)
		if(row.name.compare(name) == 0)
			return &row;
	_table.push_back(result(name,"decimal",0,0));
	return &_table.back();
}
void ResultDB::Export(std::string filename)
{
	if(filename.compare("") == 0)
	{
		std::cerr << "Filename cannot be empty." << std::endl;
		return;
	}
	std::cout << "Exporting LaTeX commands to " << filename << std::endl;
	if(filename.find(".tex") == std::string::npos)
		std::cerr << "Warning: this should really be a .tex file." << std::endl;
	std::ofstream output(filename);
	for(auto row : _table)
	{
		format_result frow(row);
		bool perc = row.type.compare("percent") == 0;
		double rawvalue = perc? row.value*100 : row.value;
		output << "%-----------------------------------------------" << std::endl;
		output << "% Ndp: " << frow.ndp << "\t Val s.f. :" << frow.nvsf << "\t Err s.f. :" << frow.nesf << std::endl;
		output << "\\def\\" << frow.macroname <<    "val{\\ensuremath{" <<                frow.value << "}\\xspace}" << std::endl;
		output << "\\def\\" << frow.macroname <<    "err{\\ensuremath{" <<                frow.error << "}\\xspace}" << std::endl;
		output << "\\def\\" << frow.macroname <<       "{\\ensuremath{" <<                frow.both  << "}\\xspace}" << std::endl;
		output << "\\def\\" << frow.macroname << "scival{\\ensuremath{" <<                frow.scval << "}\\xspace}" << std::endl;
		output << "\\def\\" << frow.macroname << "scierr{\\ensuremath{" <<                frow.scerr << "}\\xspace}" << std::endl;
		output << "\\def\\" << frow.macroname <<    "sci{\\ensuremath{" <<                frow.scbo  << "}\\xspace}" << std::endl;
		output << "\\def\\" << frow.macroname <<  "nodps{\\ensuremath{" << rdb::tostring(rawvalue,0) << "}\\xspace}" << std::endl;
		output << "\\def\\" << frow.macroname <<  "onedp{\\ensuremath{" << rdb::tostring(rawvalue,1) << "}\\xspace}" << std::endl;
		output << "\\def\\" << frow.macroname <<  "twodp{\\ensuremath{" << rdb::tostring(rawvalue,2) << "}\\xspace}" << std::endl;
	}
}
void ResultDB::Print(std::string name)
{
	for(auto row: _table)
	{
		if(name.compare(row.name) == 0)
		{
			std::cout << "rawvalue  :\t" << row.value << std::endl;
			std::cout << "rawerror  :\t" << row.error << std::endl;
			format_result frow(row);
			std::cout << "value     :\t" << frow.value << std::endl;
			std::cout << "error     :\t" << frow.error << std::endl;
			std::cout << "result    :\t" << frow.both  << std::endl;
			std::cout << "sci value :\t" << frow.scval << std::endl;
			std::cout << "sci error :\t" << frow.scerr << std::endl;
			std::cout << "sci result:\t" << frow.scbo  << std::endl;
		}
	}
}
format_result::format_result(const result& row)
{
	bool perc = row.type.compare("percent") == 0;
	double factor = perc ? 100 : 1;
	double val = row.value*factor, err = row.error*factor;
	std::string str = row.name;
	std::regex pat("[^A-Za-z]");
	macroname = std::regex_replace(str, pat, std::string(""));
	int ov = rdb::order(val);
	int oe = rdb::order(err);
	int e3sf;
	if(std::abs(err)<1e-100)
	{
		if(ov<2) val = rdb::roundSF(val, 3); // Round to 3sf if no error and order < 2
		else val = rdb::roundDP(val, 0); // If no error and order > 2, round to nearest int
		err = 0;
		error = "0";
		scerr = "0";
		ndp = 2-ov;
		nvsf = 3;
		nesf = 0;
	}
	else
	{
		e3sf = floor(err * pow(10, 3 - ceil(log10(std::abs(err))))); // first 3 significant digits of the error as a 3-digit int
		nesf = 1;
		if(e3sf < 100)
		{
			std::cerr << "Something is wrong with the rounding" << std::endl;
		}
		else if(e3sf < 355)
		{
			nesf = 2;
		}
		else if(e3sf < 950)
		{
			nesf = 1;
		}
		else
		{
			err = rdb::roundSF(err,1);
			nesf = 2;
			oe++;
		}
		ndp   = oe<0 ? nesf-1-oe : 0;
		nvsf  = nesf + ov - oe;
		val   = rdb::roundSF(val,nvsf);
		err   = rdb::roundSF(err,nesf);
		error = rdb::tostring(err,ndp);
		scerr = rdb::scinot(err,nesf-1);
	}
	value = rdb::tostring(val,ndp);
	scval = rdb::scinot(val,nvsf-1);
	both  = value + " \\pm " + error;
	scbo  = rdb::scinot(val,err,nvsf-1);
}
