#include <cmath>
#include "datum.h"
datum::datum() : value(0.), error(0.)
{
}
datum::datum(double val) : value(val), error(0.)
{
}
datum::datum(double val, double err) : value(val), error(err)
{
}
datum::datum(const datum& other) : value(other.value), error(other.error)
{
}
datum::datum(const std::pair<double,double>& valerr) : value(valerr.first), error(valerr.second)
{
}
datum::~datum()
{
}
datum datum::operator+(const datum& rhs) const
{
	return datum(value + rhs.value, std::sqrt(error * error + rhs.error * rhs.error));
}
datum datum::operator-(const datum& rhs) const
{
	return datum(value - rhs.value, std::sqrt(error * error + rhs.error * rhs.error));
}
datum datum::operator*(const datum& rhs) const
{
	const double newval = value * rhs.value;
	return datum(newval, newval * std::sqrt(error * error / (value * value) + rhs.error * rhs.error / (rhs.value * rhs.value)));
}
datum datum::operator/(const datum& rhs) const
{
	const double newval = value / rhs.value;
	return datum(newval, newval * std::sqrt(error * error / (value * value) + rhs.error * rhs.error / (rhs.value * rhs.value)));
}
datum datum::operator+=(const datum& rhs)
{
	*this = *this + rhs;
	return *this;
}
datum datum::operator-=(const datum& rhs)
{
	*this = *this - rhs;
	return *this;
}
datum datum::operator*=(const datum& rhs)
{
	*this = *this * rhs;
	return *this;
}
datum datum::operator/=(const datum& rhs)
{
	*this = *this / rhs;
	return *this;
}
datum operator+(const double lhs, const datum& rhs)
{
	return datum(lhs) + rhs;
}
datum operator-(const double lhs, const datum& rhs)
{
	return datum(lhs) - rhs;
}
datum operator*(const double lhs, const datum& rhs)
{
	return datum(lhs) * rhs;
}
datum operator/(const double lhs, const datum& rhs)
{
	return datum(lhs) / rhs;
}
datum pow(const datum& lhs, const double rhs)
{
	const double newval = std::pow(lhs.val(), rhs);
	return datum(newval, std::abs(newval * rhs * lhs.err() / lhs.val()));
}
datum sqrt(const datum& rhs)
{
	return pow(rhs,0.5);
}
std::ostream& operator<<(std::ostream& os, const datum& rhs)
{
	os << rhs.val() << " ± " << rhs.err() << std::ends;
	return os;
}
double datum::val() const
{
	return value;
}
double datum::err() const
{
	return error;
}

