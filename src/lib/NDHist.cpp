#include "NDHist.h"
#include <stdexcept>
#include <iostream>
#include <algorithm>
#include <numeric>
// Default constructor
NDHist::NDHist()
	: nbins(0)
	, under(0)
	, over(0)
{
}
// Copy constructor
NDHist::NDHist(const NDHist& orig)
	: nbins(orig.nbins)
	, under(orig.under)
	, over(orig.over)
	, bincontent(orig.bincontent)
{
}
void NDHist::Fill(const std::vector<double>& x, double weight)
{
	if(!this->CheckDim(x.size()))
		throw std::runtime_error("NDHist ERROR: Datapoint has the wrong dimension.");
	int bin = this->FindBin(x); // make sure you get the function from the right class
	if(bin < 0)
		under+=weight;
	else if(bin >= nbins)
		over+=weight;
	else
		bincontent[bin]+=weight;
}
double NDHist::Eval(const std::vector<double>& x) const
{
	return bincontent[FindBin(x)];
}
void NDHist::Clear()
{
	for(auto &binc: bincontent)
		binc = 0;
	under = 0;
	over = 0;
}
double NDHist::MaxBinContent() const
{
	double maxbincontent = bincontent[0];
	for(auto binc: bincontent)
		if(binc > maxbincontent)
			maxbincontent = binc;
	return maxbincontent;
}
double NDHist::MinBinContent() const
{
	double minbincontent = bincontent[0];
	for(auto binc: bincontent)
		if(binc < minbincontent)
			minbincontent = binc;
	return minbincontent;
}
TH1D* NDHist::BinContentHist() const
{
	return BinContentHist("BinContentHist");
}
TH1D* NDHist::BinContentHist(std::string name) const
{
	TH1D* hist = new TH1D(name.c_str(),"",100,MinBinContent(),MaxBinContent());
	for(auto binc: bincontent)
		hist->Fill(binc);
	return hist;
}

// Arithmetic with histograms
bool NDHist::Add(const NDHist& other)
{
	return Arithmetic(other,std::plus<double>());
}
bool NDHist::Subtract(const NDHist& other)
{
	return Arithmetic(other,std::minus<double>());
}
bool NDHist::Multiply(const NDHist& other)
{
	return Arithmetic(other,std::multiplies<double>());
}
bool NDHist::Divide(const NDHist& other)
{
	return Arithmetic(other,std::divides<double>());
}
template <typename F>
bool NDHist::Arithmetic(const NDHist& other,const F& op)
{
	if(!IsCompatible(other))
		throw std::runtime_error("NDHist ERROR: Histograms must have the same ranges and binning schemes to do arithmetic.");
	under = op(under, other.under);
	over = op(over, other.over);
	std::transform(bincontent.begin(), bincontent.end(), other.bincontent.begin(), bincontent.begin(), op);
	return true;
}
void NDHist::Print() const
{
	for(auto binc: bincontent)
		std::cout << binc << std::endl;
}
double NDHist::Integral() const
{
	return std::accumulate(bincontent.begin(), bincontent.end(), 0);
}

