// Standard headers
#include <string>
#include <iostream>
#include <algorithm>
// BOOST headers
#include "boost/program_options.hpp"
// ROOT headers
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TMath.h"
#include "TLine.h"
#include "TStyle.h"
// Custom headers
#include "MakeBranchPlot.h"
#include "plotmaker.h"

int main(int argc, char* argv[])
{
	using namespace boost::program_options;
	options_description desc("Allowed options",(unsigned)120);
	std::string MCfile, CDfile, branch, xtitle, unit, plotname, blurb, MCcuts, MCweight;
	double xlow, xup, lineat, MCnorm, CDnorm;
	int nbins;
	desc.add_options()
		("help"    ,                                                                                "produce help message"             )
		("pulls"   ,                                                                                "draw pull plot"                   )
		("blurb", value<std::string>(&blurb), "blurb text")
		("MCfile"  , value<std::string>(&MCfile  )->default_value("ntuples/BsphiKK_MC_mva.root"  ), "Monte Carlo file"                 )
		("CDfile"  , value<std::string>(&CDfile  )->default_value("ntuples/BsphiKK_data_mva.root"), "collision data file"              )
		("MCweight", value<std::string>(&MCweight)->default_value(""                             ), "Monte Carlo weighting variable"   )
		("MCnorm"  , value<double     >(&MCnorm  )->default_value(-1                             ), "Monte Carlo normalisation"        )
		("CDnorm"  , value<double     >(&CDnorm  )->default_value(-1                             ), "collision data normalisation"     )
		("MCcuts"  , value<std::string>(&MCcuts  )->default_value(""                             ), "Monte Carlo cuts"                 )
		("branch"  , value<std::string>(&branch  )->default_value("B_s0_LOKI_Mass"               ), "Monte Carlo branch to plot"       )
		("title"   , value<std::string>(&xtitle  )->default_value("#it{m}(#it{#phi K^{+}K^{-}})" ), "x-axis title"                     )
		("unit"    , value<std::string>(&unit    )->default_value("MeV/#it{c}^{2}"               ), "unit"                             )
		("plot"    , value<std::string>(&plotname)->default_value("comparison"                   ), "output plot filename"             )
		("auto"    ,                                                                                "automatic plot range"             )
		("lineat"  , value<double     >(&lineat  )                                                , "draw vertical line at this value" )
		("MChist"  ,                                                                                "read the MC as a histogram too"   )
	;
	variables_map vmap;
	store(parse_command_line(argc, argv, desc), vmap);
	notify(vmap);
	if (vmap.count("help"))
	{
		std::cout << desc << std::endl;
		return 1;
	}
	TH1D* CDhist = (TH1D*)(TFile::Open(CDfile.c_str())->Get(branch.c_str()));
	TH1D* MChist;
	if (vmap.count("MChist"))
	{
		MChist = (TH1D*)(TFile::Open(MCfile.c_str())->Get(branch.c_str()));
	}
	else
	{
		xlow = CDhist->GetXaxis()->GetXmin();
		xup  = CDhist->GetXaxis()->GetXmax();
		nbins = CDhist->GetNbinsX();
		MChist = MakeBranchPlot(MCfile,branch,MCcuts,MCweight,xlow,xup,nbins,false);
	}
	MChist->Scale(MCnorm<0? 1.0/MChist->Integral() : 1.0/MCnorm);
	CDhist->Scale(CDnorm<0? 1.0/CDhist->Integral() : 1.0/CDnorm);
	MChist->SetFillColor(kOrange);
	MChist->SetLineColor(kOrange);
	MChist->SetMaximum(std::max(CDhist->GetMaximum(),MChist->GetMaximum())*1.3);
	MChist->SetMinimum(0);
	// Draw everything
	plotmaker<TH1> plotter(MChist);
	TH1D pullplot;
	if(vmap.count("pulls"))
	{
		pullplot = plotmaker<TH1>::MakePullPlot(MChist, CDhist);
		plotter.SetPullPlot(&pullplot);
		gStyle->SetOptStat(1);
		gStyle->SetOptFit(1);
		plotter.DrawPullDistribution(true)->SaveAs((plotname+"_pull_dist.pdf").c_str());
	}
	plotter.SetBlurb(blurb);
	plotter.SetTitle(xtitle, unit);
	TCanvas* plot = plotter.Draw("HIST");
	CDhist->Draw("sameE1");
	TLine cutvalline;
	if(vmap.count("lineat"))
	{
		cutvalline = TLine(lineat,0,lineat,MChist->GetMaximum());
		cutvalline.SetLineStyle(2);
		cutvalline.SetLineColor(2);
		cutvalline.Draw();
	}
	plot->SaveAs((plotname+".pdf").c_str());
	plot->SaveAs((plotname+".png").c_str());
	return 0;
}
