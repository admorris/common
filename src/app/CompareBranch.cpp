// Standard headers
#include <string>
#include <iostream>
#include <algorithm>
// BOOST headers
#include "boost/program_options.hpp"
// ROOT headers
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TMath.h"
#include "TLine.h"
// Custom headers
#include "MakeBranchPlot.h"
#include "plotmaker.h"
int main(int argc, char* argv[])
{
	using namespace boost::program_options;
	options_description desc("Allowed options",(unsigned)120);
	std::string MCfile, CDfile, MCbranch, CDbranch, MCcuts, CDcuts, xtitle, unit, plotname, MCweight, CDweight, blurb;
	double xlow, xup, lineat, MCnorm, CDnorm;
	int nbins;
	desc.add_options()
		("help"    ,                                                                                "produce help message"             )
		("pulls"   ,                                                                                "draw pull plot"                   )
		("blurb", value<std::string>(&blurb), "blurb text")
		("MCfile"  , value<std::string>(&MCfile  )->default_value("ntuples/BsphiKK_MC_mva.root"  ), "Monte Carlo file"                 )
		("CDfile"  , value<std::string>(&CDfile  )->default_value("ntuples/BsphiKK_data_mva.root"), "collision data file"              )
		("MCbranch", value<std::string>(&MCbranch)->default_value("B_s0_LOKI_Mass"               ), "Monte Carlo branch to plot"       )
		("CDbranch", value<std::string>(&CDbranch)->default_value("B_s0_LOKI_Mass"               ), "collision data branch to plot"    )
		("MCweight", value<std::string>(&MCweight)->default_value(""                             ), "Monte Carlo weighting variable"   )
		("CDweight", value<std::string>(&CDweight)->default_value(""                             ), "collision data weighting variable")
		("MCnorm"  , value<double     >(&MCnorm  )->default_value(-1                             ), "Monte Carlo normalisation"        )
		("CDnorm"  , value<double     >(&CDnorm  )->default_value(-1                             ), "collision data normalisation"     )
		("MCcuts"  , value<std::string>(&MCcuts  )->default_value(""                             ), "Monte Carlo cuts"                 )
		("CDcuts"  , value<std::string>(&CDcuts  )->default_value(""                             ), "collision data cuts"              )
		("title"   , value<std::string>(&xtitle  )->default_value("#it{m}(#it{#phi K^{+}K^{-}})" ), "x-axis title"                     )
		("unit"    , value<std::string>(&unit    )->default_value("MeV/#it{c}^{2}"               ), "unit"                             )
		("plot"    , value<std::string>(&plotname)->default_value("comparison"                   ), "output plot filename"             )
		("upper"   , value<double     >(&xup     )->default_value(5600                           ), "branch upper limit"               )
		("lower"   , value<double     >(&xlow    )->default_value(5200                           ), "branch lower limit"               )
		("auto"    ,                                                                                "automatic plot range"             )
		("bins"    , value<int        >(&nbins   )->default_value(20                             ), "number of bins"                   )
		("lineat"  , value<double     >(&lineat  )                                                , "draw vertical line at this value" )
	;
	variables_map vmap;
	store(parse_command_line(argc, argv, desc), vmap);
	notify(vmap);
	if (vmap.count("help"))
	{
		std::cout << desc << std::endl;
		return 1;
	}
	bool autorange = vmap.count("auto");
	bool pulls = vmap.count("pulls");
	bool drawline = vmap.count("lineat");
	TH1D*  MChist = MakeBranchPlot(MCfile,MCbranch,MCcuts,MCweight,xlow,xup,nbins,autorange);
	if(autorange)
	{
		xlow = MChist->GetXaxis()->GetXmin();
		xup  = MChist->GetXaxis()->GetXmax();
	}
	TH1D*  CDhist = MakeBranchPlot(CDfile,CDbranch,CDcuts,CDweight,xlow,xup,nbins,false);
	MChist->Scale(MCnorm<0? 1.0/MChist->Integral() : 1.0/MCnorm);
	CDhist->Scale(CDnorm<0? 1.0/CDhist->Integral() : 1.0/CDnorm);
	MChist->SetFillColor(kOrange);
	MChist->SetLineColor(kOrange);
	MChist->SetMaximum(std::max(CDhist->GetMaximum(),MChist->GetMaximum())*1.3);
	MChist->SetMinimum(0);
	// Draw everything
	plotmaker<TH1> plotter(MChist);
	TH1D pullplot;
	if(pulls)
	{
		pullplot = plotmaker<TH1>::MakePullPlot(MChist, CDhist);
		plotter.SetPullPlot(&pullplot);
	}
	plotter.SetBlurb(blurb);
	plotter.SetTitle(xtitle, unit);
	TCanvas* plot = plotter.Draw("HIST");
	CDhist->Draw("sameE1");
	TLine cutvalline;
	if(drawline)
	{
		cutvalline = TLine(lineat,0,lineat,MChist->GetMaximum());
		cutvalline.SetLineStyle(2);
		cutvalline.SetLineColor(2);
		cutvalline.Draw();
	}
	plot->SaveAs((plotname+".pdf").c_str());
}

