// Standard headers
#include <string>
#include <iostream>
// BOOST headers
#include "boost/program_options.hpp"
// ROOT headers
#include "TFile.h"
// Custom headers
#include "annotation.h"
#include "plotmaker.h"
#include "MakeBranchPlot.h"
#include "GetTree.h"
using std::string;
using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
	using namespace boost::program_options;
	options_description desc("Allowed options",120);
	string file, branch, cuts, xtitle, unit, plotname, weight, overlayfile, blurb;
	double xlow, xup, scale, lineat;
	int nbins;
	desc.add_options()
		("help,H"  ,                                                              "produce help message"                )
		("root"    ,                                                              "save plot as .root file"             )
		("unitarea",                                                              "normalise to unit area"              )
		("blurb"   , value<string>(&blurb      )                                , "blurb text"                          )
		("file,F"  , value<string>(&file       )->required()                    , "data file"                           )
		("branch,B", value<string>(&branch     )->required()                    , "branch to plot"                      )
		("weight,W", value<string>(&weight     )                                , "weighting variable"                  )
		("cuts,C"  , value<string>(&cuts       )                                , "optional cuts"                       )
		("title,T" , value<string>(&xtitle     )                                , "x-axis title"                        )
		("unit,U"  , value<string>(&unit       )                                , "unit"                                )
		("plot,O"  , value<string>(&plotname   )->default_value("plottedbranch"), "output plot filename"                )
		("upper,u" , value<double>(&xup        )                                , "branch upper limit"                  )
		("lower,l" , value<double>(&xlow       )                                , "branch lower limit"                  )
		("auto"    ,                                                              "automatic plot range"                )
		("bins,b"  , value<int   >(&nbins      )->default_value(50)             , "number of bins"                      )
		("overlay" , value<string>(&overlayfile)                                , "file to overlay the same branch from")
		("scale"   , value<double>(&scale      )->default_value(1)              , "scale factor for the overlaid branch")
		("lineat"  , value<double>(&lineat     )                                , "draw vertical line at this value"    )
	;
	variables_map vmap;
	store(parse_command_line(argc, argv, desc), vmap);
	if (vmap.count("help"))
	{
		cout << desc << endl;
		return 1;
	}
	notify(vmap);
//	PlotBranch(file,branch,xtitle,unit,plot,cuts,weight,xlow,xup,nbins,overlay,scale,vmap.count("root"),vmap.count("lineat"),lineat,vmap.count("unitarea"),blurb,vmap.count("upper") == 0, vmap.count("lower") == 0);
	bool drawline = vmap.count("lineat") > 0;
	bool saveasrootfile = vmap.count("root");
	bool defaultup = vmap.count("upper") == 0;
	bool defaultlow = vmap.count("lower") == 0;
	if(defaultup || defaultlow)
	{
		auto tree = GetTree(file);
		if(defaultup)
			xup = tree->GetMaximum(branch.c_str());
		if(defaultlow)
			xlow = tree->GetMinimum(branch.c_str());
	}
	TH1D* frame = MakeBranchPlot(file, branch, cuts, weight, xlow, xup, nbins, vmap.count("auto"));
	if(vmap.count("unitarea") > 0) frame->Scale(1.0/frame->Integral());
	string hist = plotname.find_last_of("/") == string::npos? plotname : plotname.substr(plotname.find_last_of("/")+1);
	frame->SetName(hist.c_str());
	frame->SetMaximum(frame->GetMaximum()*1.3);
	frame->SetMinimum(0);
	plotmaker<TH1> plotter(frame);
	plotter.SetBlurb(blurb);
	plotter.SetTitle(xtitle, unit);
	TCanvas* plot = plotter.Draw("E1");
	if(overlayfile!="")
	{
		if(vmap.count("auto") > 0)
		{
			xlow = frame->GetXaxis()->GetXmin();
			xup  = frame->GetXaxis()->GetXmax();
		}
		TH1D* overlay = MakeBranchPlot(overlayfile, branch, cuts, weight, xlow, xup, nbins, false);
		if(scale>0) overlay->Scale(scale);
		else overlay->Scale(frame->Integral()/overlay->Integral());
		overlay->SetDrawOption("B");
		overlay->SetFillColor(kOrange);
		overlay->SetLineColor(kOrange);
		overlay->Draw("same");
		frame->Draw("E1 same");
	}
	TLine cutvalline;
	if(drawline)
	{
		cutvalline = TLine(lineat,0,lineat,frame->GetMaximum());
		cutvalline.SetLineStyle(2);
		cutvalline.SetLineColor(2);
		cutvalline.Draw();
	}
	plot->SaveAs((plotname+".pdf").c_str());
	if(saveasrootfile)
	{
		TFile* file = TFile::Open((hist+".root").c_str(),"RECREATE");
		frame->Write();
		plot->Write();
		file->Close();
	}
	return 0;
}
