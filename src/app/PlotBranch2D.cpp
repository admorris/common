// Standard headers
#include <string>
#include <iostream>
// BOOST headers
#include "boost/program_options.hpp"
// ROOT headers
#include "TFile.h"
#include "TStyle.h"
// Custom headers
#include "annotation.h"
#include "plotmaker.h"
#include "MakeBranchPlot.h"
#include "GetTree.h"

void settitle(TAxis* axis, std::string title, std::string unit)
{
	if(unit.empty())
		axis->SetTitle(("#font[132]{}"+title).c_str());
	else
		axis->SetTitle(("#font[132]{}"+title+" #font[132]{}["+unit+"]").c_str());
}

int main(int argc, char* argv[])
{
	using namespace boost::program_options;
	options_description desc("Allowed options",120);
	std::string file, xbranch, ybranch, cuts, xtitle, ytitle, xunit, yunit, plotname, weight, drawopt;
	double xlow, xup, ylow, yup;
	int nxbins, nybins;
	desc.add_options()
		("help,H"  ,                                                                "produce help message"        )
		("root"    ,                                                                "save plot as .root file"     )
		("file,F"  , value<std::string>(&file    )->required()                    , "data file"                   )
		("x-branch", value<std::string>(&xbranch )->required()                    , "branch to plot on the x axis")
		("y-branch", value<std::string>(&ybranch )->required()                    , "branch to plot on the y axis")
		("weight,W", value<std::string>(&weight  )                                , "weighting variable"          )
		("cuts,C"  , value<std::string>(&cuts    )                                , "optional cuts"               )
		("x-title" , value<std::string>(&xtitle  )                                , "x-axis title"                )
		("y-title" , value<std::string>(&ytitle  )                                , "y-axis title"                )
		("x-unit"  , value<std::string>(&xunit   )                                , "x-axis unit"                 )
		("y-unit"  , value<std::string>(&yunit   )                                , "y-axis unit"                 )
		("plot,O"  , value<std::string>(&plotname)->default_value("plottedbranch"), "output plot filename"        )
		("drawopt" , value<std::string>(&drawopt )->default_value("COLZ")         , "draw option"                 )
		("x-upper" , value<double>(&xup     )                                     , "x branch upper limit"        )
		("x-lower" , value<double>(&xlow    )                                     , "x branch lower limit"        )
		("y-upper" , value<double>(&yup     )                                     , "y branch upper limit"        )
		("y-lower" , value<double>(&ylow    )                                     , "y branch lower limit"        )
		("x-bins"  , value<int   >(&nxbins  )->default_value(100)                 , "number of bins on x-axis"    )
		("y-bins"  , value<int   >(&nybins  )->default_value(100)                 , "number of bins on y-axis"    )
	;
	variables_map vmap;
	store(parse_command_line(argc, argv, desc), vmap);
	if (vmap.count("help"))
	{
		std::cout << desc << std::endl;
		return 1;
	}
	notify(vmap);
//	PlotBranch(file,branch,xtitle,unit,plot,cuts,weight,xlow,xup,nbins,overlay,scale,vmap.count("root"),vmap.count("lineat"),lineat,vmap.count("unitarea"),blurb,vmap.count("upper") == 0, vmap.count("lower") == 0);
	bool saveasrootfile = vmap.count("root");
	bool xdefaultup = vmap.count("x-upper") == 0;
	bool xdefaultlow = vmap.count("x-lower") == 0;
	bool ydefaultup = vmap.count("y-upper") == 0;
	bool ydefaultlow = vmap.count("y-lower") == 0;
	if(xdefaultup || xdefaultlow
	|| ydefaultup || ydefaultlow)
	{
		auto tree = GetTree(file);
		if(xdefaultup)
			xup = tree->GetMaximum(xbranch.c_str());
		if(xdefaultlow)
			xlow = tree->GetMinimum(xbranch.c_str());
		if(ydefaultup)
			yup = tree->GetMaximum(ybranch.c_str());
		if(ydefaultlow)
			ylow = tree->GetMinimum(ybranch.c_str());
	}
	gStyle->SetOptStat(0);
	TCanvas plot("plot","",1400,1200);
	TH2D* frame = MakeBranchPlot2D(file, xbranch, ybranch, cuts, weight, xlow, xup, nxbins, ylow, yup, nybins);
	std::string hist = plotname.find_last_of("/") == string::npos? plotname : plotname.substr(plotname.find_last_of("/")+1);
	frame->Draw(drawopt.c_str());
	settitle(frame->GetXaxis(), xtitle, xunit);
	settitle(frame->GetYaxis(), ytitle, yunit);
	for(TAxis* axis: {frame->GetXaxis(), frame->GetYaxis()})
	{
		axis->SetTitleFont(132);
		axis->SetLabelFont(132);
		axis->SetNdivisions(505);
		axis->SetTitleOffset(1.10);
		axis->SetTitleSize	(0.065);
		axis->SetLabelSize	(0.055);
		axis->SetLabelOffset(0.015);
	}
	plot.SetMargin(0.15,0.10,0.15,0.02);
	plot.SaveAs((plotname+".pdf").c_str());
	if(saveasrootfile)
	{
		TFile* file = TFile::Open((hist+".root").c_str(),"RECREATE");
		frame->Write();
		plot.Write();
		file->Close();
	}
	return 0;
}
