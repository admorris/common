#ifndef __ResultDB_h__
#define __ResultDB_h__
#include <string>
#include <vector>
#include <cmath>
#include <sstream>
#include <iomanip>
namespace rdb
{
	int order(double value);
	double roundSF(double value, int nsf);
	double roundDP(double value, int ndp);
	template<typename T>
	std::string tostring(T value, int prec);
	std::string scinot(double value, int ndp);
	std::string scinot(double value, double error, int ndp);
}
struct result
{
	result();
	result(std::string, std::string, double, double);
	void set(std::string, double, double);
	std::string name;
	std::string type;
	double value;
	double error;
};
struct format_result
{
	format_result(const result&);
	std::string value;
	std::string error;
	std::string both;
	std::string scval;
	std::string scerr;
	std::string scbo;
	std::string macroname;
	int ndp, nvsf, nesf;
};
class ResultDB
{
	public:
		ResultDB();
		ResultDB(std::string);
		~ResultDB();
		bool Open(std::string);
		bool Save(std::string filename = "");
		bool Close();
		void Update(std::string, std::string, double, double);
		result Get(std::string resultname){ return *find(resultname); }
		void Export(std::string);
		void Print(std::string);
	private:
		result* find(std::string);
		std::string _filename;
		std::vector<result> _table;
		bool _isopen;
		bool _ismod;
};
#endif
