#include <ostream>
#include <utility>
class datum
{
	public:
		datum();
		datum(double val);
		datum(double val, double err);
		datum(const std::pair<double,double>& valerr);
		datum(const datum& other);
		~datum();
		datum operator+(const datum& rhs) const;
		datum operator-(const datum& rhs) const;
		datum operator*(const datum& rhs) const;
		datum operator/(const datum& rhs) const;
		datum operator+=(const datum& rhs);
		datum operator-=(const datum& rhs);
		datum operator*=(const datum& rhs);
		datum operator/=(const datum& rhs);
		double val() const;
		double err() const;
	private:
		double value;
		double error;
};
datum pow(const datum& lhs, const double rhs);
datum sqrt(const datum& rhs);
std::ostream& operator<<(std::ostream& os, const datum& rhs);
datum operator+(const double lhs, const datum& rhs);
datum operator-(const double lhs, const datum& rhs);
datum operator*(const double lhs, const datum& rhs);
datum operator/(const double lhs, const datum& rhs);


// make an operator-like thingy <pm> to allow more natural syntax like
// datum somenumber = 12.3 <pm> 0.2
namespace plusminus
{
	enum {pm};
}
struct LHS
{
	LHS(double _value) : value(_value) {}
	double value;
};
// convert 'double < pm' into a LHS
LHS operator<(const double& lhs, decltype(plusminus::pm))
{
	return lhs;
}
// convert 'LHS > double' into a datum
datum operator>(LHS lhs, double rhs)
{
	return datum(lhs.value, rhs);
}
// Stop compiler warnings about unused operators in a header file (wtf?)
datum plusminusexample()
{
	using plusminus::pm;
	return 0.9 <pm> 0.1;
}

