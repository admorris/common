#ifndef __MakeBranchPlot_h__
#define __MakeBranchPlot_h__
#include <string>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TFile.h"
using std::string;
TH1D* MakeBranchPlot(TTree&, string, string, string, double, double, int, bool);
TH1D* MakeBranchPlot(TFile&, string, string, string, double, double, int, bool);
TH1D* MakeBranchPlot(string, string, string, string, double, double, int, bool);
TH2D* MakeBranchPlot2D(TTree&, string, string, string, string, double, double, int, double, double, int);
TH2D* MakeBranchPlot2D(TFile&, string, string, string, string, double, double, int, double, double, int);
TH2D* MakeBranchPlot2D(string, string, string, string, string, double, double, int, double, double, int);
#endif
