#ifndef __PLOTMAKER_H__
#define __PLOTMAKER_H__
#include "TCanvas.h"
#include "TPad.h"
#include "TH1.h"
#include "TGraph.h"
#include "RooPlot.h"
#include "TLatex.h"
#include <string>
using std::string;
template<class T>
class plotmaker
{
	private:
		void     init();
		void     getaxes(T*, bool);
		void     makepads();
		// The blurb
		TLatex*  _blurb;
		string   _blurbtext;
		double   _blurbx;
		double   _blurby;
		// Objects to draw on
		TCanvas* _canvas;
		TPad*    _mainpad;
		TPad*    _pullpad;
		// The plots
		T*       _mainplot;
		T*       _pullplot;
		// The Axes
		TAxis*   _mainxaxis;
		TAxis*   _mainyaxis;
		TAxis*   _pullxaxis;
		TAxis*   _pullyaxis;
		// Axis label & unit
		string   _xtitle;
		string   _unit;
		bool     _dimensionless;
		bool     _logy;
		string   _drawopt;
		// Keep track whether or not a pull plot has been given
		bool     _usepull;
		// Apply LHCb paper style to plot
		void     setxtitle(TAxis*);
		void     setytitle(TAxis*,double);
		void     stylemainaxis(TAxis*);
		void     stylepullaxes(TAxis*,TAxis*);
		void     drawplot(T*,string option = "");
		void     drawblurb();
		void     makesymmetric(T*);
		// Helpers to make pull plots
		static void normaliseresiduals(TH1*, const TH1*, const TH1*);
	public:
		// Constructors
		plotmaker(T*);
		~plotmaker();
		// Get and set variables
		TCanvas* GetCanvas() { return _canvas; }
		string   GetBlurb()   { return _blurbtext; }
		void     SetLogy(bool logy = true) { _logy = logy; }
		void     SetDrawOption(string);
		void     SetBlurb(string);
		void     SetBlurbPosition(double,double);
		void     SetMinimum(double min) { _mainplot->SetMinimum(min); }
		void     SetMaximum(double max) { _mainplot->SetMaximum(max); }
		void     SetPullPlot(T*);
		void     SetTitle(string,string);
		// Do stuff
		static TH1D MakePullPlot(TH1D*, TH1D*);
		static int GetNbins(T*);
		static auto GetY(T*, int);
		static auto GetEY(T*, int);
		TCanvas* DrawPullDistribution(bool fit = true);
		TCanvas* Draw(string option = "");
};
#endif
