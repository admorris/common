cmake_minimum_required(VERSION 3.0)
project(common)

find_package(Boost REQUIRED program_options)
find_package(ROOT REQUIRED Core RooFit)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY lib)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ROOT_CXX_FLAGS}")
include_directories(${ROOT_INCLUDE_DIRS} ${common_SOURCE_DIR}/include)

set(LIBSRCLIST annotation
CutEff
datum
GetTree
LegendreMomentPlot
MakeBranchPlot
NDHist
NDHist_Adaptive
NDHist_Fixed
plotmaker
progbar
ResultDB
substitute)

foreach(src ${LIBSRCLIST})
	add_library(${src} SHARED ${common_SOURCE_DIR}/src/lib/${src}.cpp)
endforeach()

#add_executable(reweight_XSLFF ${FFRW_SOURCE_DIR}/src/reweight_XSLFF.cpp)
#target_link_libraries(reweight_XSLFF XslFFReweighting ${ROOT_LIBRARIES} ${Boost_LIBRARIES})

set(BINSRCLIST CalcEff
CompareBranch
CompareBranchRatio
CompareBranch_ReadHist
CompareMoments
ExportResults
GetCutEff
GetEntries
PlotBranch2D
PlotBranch
PlotMoments
PrintResult
ROCcurve
TestResultDB
UpdateResults
)
foreach(src ${BINSRCLIST})
	add_executable(${src} ${common_SOURCE_DIR}/src/app/${src}.cpp)
	target_link_libraries(${src} ${ROOT_LIBRARIES} ${Boost_LIBRARIES} GetTree)
endforeach()

foreach(src CalcEff ExportResults GetCutEff PrintResult TestResultDB UpdateResults)
	target_link_libraries(${src} ResultDB datum)
endforeach()

foreach(src GetCutEff ROCcurve)
	target_link_libraries(${src} CutEff)
endforeach()

foreach(src CompareBranch CompareBranchRatio CompareBranch_ReadHist CompareMoments PlotBranch PlotBranch2D PlotMoments ROCcurve)
	target_link_libraries(${src} MakeBranchPlot plotmaker)
endforeach()


foreach(src CompareMoments PlotMoments)
	target_link_libraries(${src} LegendreMomentPlot)
endforeach()

target_link_libraries(ROCcurve substitute)

